//@flow

import React, { PropTypes } from 'react';
import {
  Container, Body, Content, Left, Right, Header, Footer, FooterTab,
  Text, Button, Title, Badge, Icon,
  List, ListItem
} from 'native-base';

export default function AppLayout(props) {
  const recentButtonIcon = recentButtonIcon === undefined ? "list" : props.recentButtonIcon;
  const recentButtonLabel = recentButtonLabel === undefined ? "Recent" : props.recentButtonLabel;

  const searchButtonIcon = searchButtonIcon === undefined ? "search" : props.searchButtonIcon;
  const searchButtonLabel = searchButtonLabel === undefined ? "Find" : props.searchButtonLabel;

  const addButtonIcon = addButtonIcon === undefined ? "add" : props.addButtonIcon;
  const addButtonLabel = addButtonLabel === undefined ? "Add" : props.addButtonLabel;

  return (
    <Container>
      <Header>
        <Left>
          <Button transparent>
            <Icon name='menu' />
          </Button>
        </Left>
        <Body>
          <Title>{props.title}</Title>
        </Body>
        <Right />
      </Header>
      <Content>
        {props.children}
      </Content>
      <Footer>
        <FooterTab>
          <Button badge vertical>
            <Badge><Text>{props.recentItemsCount}</Text></Badge>
            <Icon name={recentButtonIcon} />
            <Text>{recentButtonLabel}</Text>
          </Button>
          <Button vertical>
            <Icon name={searchButtonIcon} />
            <Text>{searchButtonLabel}</Text>
          </Button>
          <Button vertical onPress={props.onAddTodoItem}>
            <Icon name={addButtonIcon} />
            <Text>{addButtonLabel}</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  );
};

AppLayout.propTypes = {
  title: PropTypes.string.isRequired,
  recentButtonIcon: PropTypes.string,
  recentItemsCount: PropTypes.number.isRequired,
  searchButtonIcon: PropTypes.string,
  recentButtonLabel: PropTypes.string,
  searchButtonLabel: PropTypes.string,
  addButtonIcon: PropTypes.string,
  onAddTodoItem: PropTypes.func.isRequired,
  addButtonLabel: PropTypes.string,
};
