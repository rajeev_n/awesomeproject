//@flow

import React, { Component, PropTypes } from 'react';
import { List } from 'native-base';
import TodoListItem from './TodoListItem';
import { TODO_ITEM } from './TodoListConstants';

export default class TodoList extends Component {
  constructor(props) {
    super(props);

    this.props = props;
    this.onRenderTodoItem = this.onRenderTodoItem.bind(this);
  }

  onRenderTodoItem(item) {
    return (
      <TodoListItem
        todoItem={item}
        onRemoveTodoItem={this.props.onRemoveTodoItem}
      />
    );
  }

  render() {
    return (
      <List
        dataArray={this.props.todos}
        renderRow={this.onRenderTodoItem}>
      </List>
    );
  }
};

TodoList.propTypes = {
  todos: PropTypes.arrayOf(PropTypes.shape(TODO_ITEM)).isRequired,
  onRemoveTodoItem : PropTypes.func.isRequired
};
