//@flow

import React, { PropTypes } from 'react';
import { Body, Right, Text, CheckBox, ListItem } from 'native-base';
import { TODO_ITEM } from './TodoListConstants'

export default function TodoListItem(props) {
  return (
    <ListItem>
      <Body>
        <Text>{props.todoItem.title}</Text>
      </Body>
      <Right>
        <CheckBox checked={false} onPress={() => { props.onRemoveTodoItem(props.todoItem) }} />
      </Right>
    </ListItem>
  );
};

TodoListItem.propTypes = {
  todoItem: PropTypes.shape(TODO_ITEM).isRequired,
  onRemoveTodoItem: PropTypes.func.isRequired,
};
