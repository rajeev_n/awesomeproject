import { PropTypes } from 'react';

export const TODO_ITEM = {
  title: PropTypes.string,
  _key: PropTypes.string,
};
