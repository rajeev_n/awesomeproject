//@flow

import React, { Component } from 'react';
import firebase from 'firebase';
import TodoAppView from './TodoAppView';

const firebaseConfig = {
  apiKey: "AIzaSyC3ebdIS2Hyt0QDL7A4_QJC0Nuu6lK6jUE",
  authDomain: "firereactbasenative.firebaseapp.com",
  databaseURL: "https://firereactbasenative.firebaseio.com",
  storageBucket: "",
};
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default class TodoApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: [],
      recentItemsCount: 0
    };
    this.itemsRef = firebaseApp.database().ref().child('items');

    this.onAddTodoItem = this.onAddTodoItem.bind(this);
    this.onRemoveTodoItem = this.onRemoveTodoItem.bind(this);
  }

  componentDidMount() {
    this.listenForTodoItems(this.itemsRef);
  }

  onAddTodoItem(item) {
    this.itemsRef.push({ title: item.title });

  }

  onRemoveTodoItem(item) {
    this.itemsRef.child(item._key).remove();
  }

  listenForTodoItems(itemsRef) {
    itemsRef.on('value', (snap) => {
      var items = [];
      snap.forEach((child) => {
        items.push({
          title: child.val().title,
          _key: child.key
        });
      });

      this.setState({
        todos: items
      });
    });
  }

  render() {
    return (
      <TodoAppView
        title="Grocery List"
        todos={this.state.todos}
        recentItemsCount={this.state.recentItemsCount}
        onAddTodoItem={this.onAddTodoItem}
        onRemoveTodoItem={this.onRemoveTodoItem}
      />
    );
  }
}
