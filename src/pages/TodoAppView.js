//@flow

import React, { PropTypes } from 'react';
import AppLayout from '../components/AppLayout'
import TodoList from '../components/TodoList';
import { TODO_ITEM } from '../components/TodoList/TodoListConstants';

export default function TodoAppView(props) {
  return (
    <AppLayout
      title={props.title}
      recentItemsCount={props.recentItemsCount}
      onAddTodoItem={props.onAddTodoItem}
    >
      <TodoList
        todos={props.todos}
        onRemoveTodoItem={props.onRemoveTodoItem}>
      </TodoList>
    </AppLayout>
  );
};

TodoAppView.propTypes = {
  title: PropTypes.string.isRequired,
  recentItemsCount: PropTypes.number.isRequired,
  onAddTodoItem: PropTypes.func.isRequired,
  todos: PropTypes.arrayOf(PropTypes.shape(TODO_ITEM)).isRequired,
  onRemoveTodoItem: PropTypes.func.isRequired
};
