//@flow

import {  AppRegistry,} from 'react-native';
import TodoApp from './src/pages/TodoApp'

AppRegistry.registerComponent('AwesomeProject', () => TodoApp);
